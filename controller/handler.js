'use strict';
module.exports.get_end_date = async (event, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    //get the postData and logged
    let postData = JSON.parse(event.body);
    console.log("postData == " + JSON.stringify(postData));

    //validate input
    if (postData.start_date) {

        //if input is validated, get the start date given
        let date = new Date(postData.start_date);

        date = new Date(date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear());

        //getting the last day of month
        let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        lastDay = lastDay.getDate() + "-" + (lastDay.getMonth() + 1) + "-" + lastDay.getFullYear();

        console.log("output date == " + lastDay)
        //return the success response with the output in response data
        return response(false, "Successfully calculated the output date", 1, lastDay);
    } else {
        //send error response for missing input
        return response(false, "Please check the input sent, start_date is missing", 0, null);
    }
}


async function response(showMessage, message, code, responseData) {
    let response = ''
    return response =
        {
            statusCode: 200,
            body: JSON.stringify({
                "showMessage": showMessage,
                "responseCode": code,
                "responseStatus": code === 1 ? "Success" : "Error",
                "responseMessage": message,
                "response": responseData
            }),
            headers:
                {
                    'Content-Type': 'application/json; charset=utf-8', // 'application/json',
                    'Access-Control-Allow-Origin': '*'
                }
        };

}
